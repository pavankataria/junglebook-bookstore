//
//  BookListTableViewController.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "BookListTableViewController.h"
#import "RSSBookEntryModel.h"
#import "BookEntryTableViewCell.h"
#import "BookDetailTableViewController.h"

@interface BookListTableViewController ()
@property (strong, nonatomic) NSMutableArray *bookEntriesDataSource;
@end

@implementation BookListTableViewController
-(NSMutableArray *)bookEntriesDataSource{
    if(!_bookEntriesDataSource) _bookEntriesDataSource = [[NSMutableArray alloc] init];
    return _bookEntriesDataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Book feed";
    [[UITabBar appearance]setBarTintColor:[UIColor whiteColor]];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(reload)
                  forControlEvents:UIControlEventValueChanged];
    
    [self reload];
}

-(void)reload{
    [SVProgressHUD showWithStatus:@"Loading feed" maskType:SVProgressHUDMaskTypeBlack];
    
    [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"fetching"]];
    [[PKAmazonClientManager sharedManager] getBookEntriesFromRSSFeedURLString:kAmazonDefaultFeedURLString withCompletionBlock:^(id responseObject, NSError *error) {
        self.bookEntriesDataSource = responseObject;
        
        
        [[PKAmazonClientManager sharedManager] getImagesForBookObjectsInArray:self.bookEntriesDataSource withCompletionBlock:^(NSError *error) {
                [[PKCoreDataManager sharedManager] saveUniqueBookEntriesWithArray:self.bookEntriesDataSource withCompletionBlock:^(NSError *error) {
                    [SVProgressHUD dismiss];
                        [self.refreshControl endRefreshing];
                       [self.refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Pull to refresh"]];
                }];
        }];
        
        [self.tableView reloadData];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.bookEntriesDataSource count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *bookEntryCellIdentifier = @"bookEntryCellIdentifier";
    BookEntryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bookEntryCellIdentifier forIndexPath:indexPath];
    
    cell.bookTitleLabel.text = [self.bookEntriesDataSource[indexPath.row] bookTitle];
    cell.bookAuthorLabel.text = [self.bookEntriesDataSource[indexPath.row] bookAuthor];
    cell.bookPriceLabel.text = [self.bookEntriesDataSource[indexPath.row] bookPrice];
    
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:kJungleBookIdentifier]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        BookDetailTableViewController *destinationViewController = segue.destinationViewController;
        destinationViewController.book = self.bookEntriesDataSource[indexPath.row];
    }

}


@end
