//
//  BookDetailTableViewController.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "BookDetailTableViewController.h"

@interface BookDetailTableViewController ()

@end

@implementation BookDetailTableViewController
#define kImageCellRow 4
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                    target:self
                                    action:@selector(shareAction)];
    
    self.navigationItem.rightBarButtonItem = shareButton;
    self.title = self.book.bookTitle;
}


-(void)shareAction{
    NSString *subject = @"Hey look at this great book I found with Junglebook!";
    NSString *body = [NSString stringWithFormat:@"I was looking around on Junglebook and found this great book - check it out! \n\n'%@' by %@", self.book.bookTitle, self.book.bookAuthor];
    
    NSArray *objectsToShare = @[body];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    [activityVC setValue:subject forKey:@"subject"];
    
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypeCopyToPasteboard];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 5;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kCellIdentifier = @"BookDetailCell";
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellIdentifier];
        if(indexPath.row != kImageCellRow){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    switch (indexPath.row) {
        case 0:
            cell.detailTextLabel.text = @"Title";
            cell.textLabel.text = self.book.bookTitle;
            break;
        case 1:
            cell.detailTextLabel.text = @"Author";
            cell.textLabel.text = self.book.bookAuthor;
            break;
        case 2:
            cell.detailTextLabel.text = @"Price";
            cell.textLabel.text = self.book.bookPrice;
            break;
        case 3:
            cell.detailTextLabel.text = @"Rating";
            cell.textLabel.text = self.book.bookRating;
            break;
        case 4:
            cell.detailTextLabel.text = @"Image Cover";
            cell.imageView.image = (UIImage*)self.book.bookCoverThumbnailImage;
            
            break;
        default:
            break;
    }
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
