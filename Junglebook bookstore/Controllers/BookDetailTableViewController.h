//
//  BookDetailTableViewController.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSBookEntryModel.h"

@interface BookDetailTableViewController : UITableViewController

@property (strong, nonatomic) RSSBookEntryModel *book;
@end
