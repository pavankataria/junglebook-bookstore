//
//  PKCoreDataManager.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKCoreDataManager : NSObject

+(instancetype)sharedManager;
-(BOOL)saveUniqueBookEntriesWithArray:(NSArray*)array withCompletionBlock:(void(^)(NSError *error))completionBlock;
@end

