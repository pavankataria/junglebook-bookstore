//
//  Constants.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "Constants.h"


//Amazon constants
NSString * const kAmazonDefaultFeedURLString = @"http://www.amazon.co.uk/gp/rss/bestsellers/books/62/ref=zg_bs_62_rsslink";
NSString * const kAmazonRootNode = @"channel";
NSString * const kAmazonStartTag = @"<startTag>";
NSString * const kAmazonEndTag = @"</startTag>";

NSString * const kAmazonFeedItemKey = @"item";
NSString *const kAmazonSpanKey = @"span";
NSString * const kAmazonDescriptionKey = @"description";
NSString *const kAmazonUnderscoreTextKey = @"__text";
NSString *const kAmazonAHREFKey = @"a";
NSString *const kAmazonBookCoverBaseURL = @"http://ecx.images-amazon.com/images/";

NSString *const kAmazonBookCoverThumbnailSize = @"._SL100";
NSString *const kAmazonBookCoverMaxSize = @"._SL500";
NSString *const kAmazonBookCoverFileExtention = @".jpg";


//Jungle Book constants
NSString * const kJungleBookIdentifier = @"showDetailViewIdentifier";
