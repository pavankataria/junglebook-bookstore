//
//  RSSBookEntryModel.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "RSSBookEntryModel.h"

@implementation RSSBookEntryModel




-(NSString*)description{
    NSString *string = [NSString stringWithFormat:@"\n<<%@>:\n%@,\n%@,\n%@,\n%@,\n%@,\n%@>", [self class], self.bookTitle, self.bookAuthor, self.bookRating, self.bookCoverImageThumbnailURL, self.bookCoverImageOriginalURL, self.bookPrice];
    return string;
}

@end
