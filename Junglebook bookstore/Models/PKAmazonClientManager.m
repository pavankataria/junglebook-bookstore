//
//  PKAmazonClientManager.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKAmazonClientManager.h"
//#import "SMXMLDocumentResponseSerializer.h"
//#import "CDataParserModel.h"


#import "PKAmazonProcessor.h"

@interface PKAmazonClientManager (){
    int currentPosition;
    BOOL inAmazonFeedItemNode;
}
@property (strong, nonatomic) NSMutableArray *xmlBookEntries;
@end
@implementation PKAmazonClientManager

static PKAmazonClientManager *_sharedClient = nil;

+(instancetype)sharedManager{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[PKAmazonClientManager alloc] init];
        _sharedClient.requestSerializer = [AFHTTPRequestSerializer serializer];
        _sharedClient.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/rss+xml"];

        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [_sharedClient.operationQueue setMaxConcurrentOperationCount:20];
        [_sharedClient resetVariables];
    });
    
    return _sharedClient;
}
-(void)resetVariables{
    currentPosition = 0;
}
-(NSMutableArray *)xmlBookEntries{
    if(!_xmlBookEntries) _xmlBookEntries = [[NSMutableArray alloc] init];
    return _xmlBookEntries;
}

-(void)getBookEntriesFromRSSFeedURLString:(NSString*)rssFeedURLString withCompletionBlock:(void(^)(id responseObject, NSError *error))completionBlock{
    [_sharedClient GET:rssFeedURLString
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   [self.xmlBookEntries removeAllObjects];
                   NSArray *bookEntries = [PKAmazonProcessor processAmazonResponseWithXMLData:responseObject];
                   
                   if(completionBlock){
                       completionBlock(bookEntries, nil);
                   }
               }
               failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   if(completionBlock){
                       completionBlock(nil, error);
                   }
               }];
}


-(void)getImagesForBookObjectsInArray:(NSArray*)array withCompletionBlock:(void (^)(NSError *error))completionBlock{
    NSMutableArray *batchOfOperations = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < [array count]; i++){
        NSURL* urlForThumbnail = [NSURL URLWithString:((RSSBookEntryModel*)array[i]).bookCoverImageThumbnailURL];
        NSMutableURLRequest *urlRequestThumbnail = [[NSMutableURLRequest alloc] initWithURL:urlForThumbnail];
        AFHTTPRequestOperation *reqThumbnailOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequestThumbnail];
        reqThumbnailOperation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [reqThumbnailOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            ((RSSBookEntryModel*)array[i]).bookCoverThumbnailImage = responseObject;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        [batchOfOperations addObject:reqThumbnailOperation];
        
        
        NSURL* urlForOriginal = [NSURL URLWithString:((RSSBookEntryModel*)array[i]).bookCoverImageOriginalURL];
        NSMutableURLRequest *urlRequestOriginal = [[NSMutableURLRequest alloc] initWithURL:urlForOriginal];
        AFHTTPRequestOperation *reqOriginalOperation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequestOriginal];
        reqOriginalOperation.responseSerializer = [AFImageResponseSerializer serializer];
        
        [reqOriginalOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            ((RSSBookEntryModel*)array[i]).bookCoverOriginalImage = responseObject;
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        [batchOfOperations addObject:reqOriginalOperation];
    }

    NSArray *operations = [AFURLConnectionOperation batchOfRequestOperations:batchOfOperations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {

    } completionBlock:^(NSArray *operations) {
        completionBlock(nil);
    }];
    [_sharedClient.operationQueue addOperations:operations waitUntilFinished:NO];
    
}
@end
