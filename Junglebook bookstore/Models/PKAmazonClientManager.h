//
//  PKAmazonClientManager.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "RSSBookEntryModel.h"

#import "XMLDictionary.h"
@interface PKAmazonClientManager : AFHTTPRequestOperationManager<NSXMLParserDelegate>


+(instancetype)sharedManager;

-(void)getBookEntriesFromRSSFeedURLString:(NSString*)rssFeedURLString withCompletionBlock:(void(^)(id responseObject, NSError *error))completionBlock;


-(void)getImagesForBookObjectsInArray:(NSArray*)array withCompletionBlock:(void (^)(NSError *error))completionBlock;

@end
