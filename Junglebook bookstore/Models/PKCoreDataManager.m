//
//  PKCoreDataManager.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKCoreDataManager.h"
#import "BookEntry.h"
#import "RSSBookEntryModel.h"

@interface PKCoreDataManager()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
@implementation PKCoreDataManager

static PKCoreDataManager *_sharedManager;
+(instancetype)sharedManager{

    static dispatch_once_t dispatchToken;
    dispatch_once(&dispatchToken, ^{
        _sharedManager = [[PKCoreDataManager alloc] init];
    });
    return _sharedManager;
}

-(id)init{
    self = [super init];
    if(!self) return nil;
    
    self.managedObjectContext = [AppDelegate getAppDelegateReference].managedObjectContext;
    self.persistentStoreCoordinator = [AppDelegate getAppDelegateReference].persistentStoreCoordinator;
    return self;
}


-(BOOL)saveUniqueBookEntriesWithArray:(NSArray*)array withCompletionBlock:(void(^)(NSError *error))completionBlock{
    NSMutableArray *possibleBooksToSaveToArray = [array mutableCopy];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([BookEntry class]) inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *booksAlreadyInDatabaseNoNeedToSaveArray = [[NSMutableArray alloc] init];
    if([result count] > 0){
        for(int i = 0; i < [possibleBooksToSaveToArray count]; i++){
            for(int k = 0; k < [result count]; k++){
                RSSBookEntryModel * currentPossibleBookEntry = possibleBooksToSaveToArray[i];
                BookEntry * currentDBObject = result[k];
                NSString *cbemTitle = [currentPossibleBookEntry.bookTitle lowercaseString];
                NSString *cbemAuthor = [currentPossibleBookEntry.bookAuthor lowercaseString];
                
                NSString *dbTitle = [currentDBObject.bookTitle lowercaseString];
                NSString *dbAuthor = [currentDBObject.bookAuthor lowercaseString];
                
                                  
                if([cbemTitle isEqualToString:dbTitle] && [cbemAuthor isEqualToString:dbAuthor]){
                    [booksAlreadyInDatabaseNoNeedToSaveArray addObject:currentPossibleBookEntry];
                    break;
                }
            }
        }
    }
    
    [possibleBooksToSaveToArray removeObjectsInArray:booksAlreadyInDatabaseNoNeedToSaveArray];
    
    for(int i = 0; i < [possibleBooksToSaveToArray count]; i++){
        BookEntry *newBookEntry = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([BookEntry class])
                                                                inManagedObjectContext:self.managedObjectContext];

        RSSBookEntryModel *bookEntryToSave = possibleBooksToSaveToArray[i];
        [newBookEntry setBookTitle:bookEntryToSave.bookTitle];
        [newBookEntry setBookAuthor:bookEntryToSave.bookAuthor];
        [newBookEntry setBookPrice:bookEntryToSave.bookPrice];
        [newBookEntry setBookRating:bookEntryToSave.bookRating];
        [newBookEntry setBookCoverOriginalImageURL:bookEntryToSave.bookCoverImageThumbnailURL];
        [newBookEntry setBookCoverThumbnailImageURL:bookEntryToSave.bookCoverImageOriginalURL];
        
        
        CGDataProviderRef thumbnailProvider = CGImageGetDataProvider(((UIImage*)bookEntryToSave.bookCoverOriginalImage).CGImage);
        NSData* thumbnailData = (id)CFBridgingRelease(CGDataProviderCopyData(thumbnailProvider));
        
        
        CGDataProviderRef originalProvider = CGImageGetDataProvider(((UIImage*)bookEntryToSave.bookCoverThumbnailImage).CGImage);
        NSData* originalData = (id)CFBridgingRelease(CGDataProviderCopyData(originalProvider));

        [newBookEntry setBookCoverOriginalImage:thumbnailData];
        [newBookEntry setBookCoverThumbnailImage:originalData];
    }
    
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    
    completionBlock(saveError);
    return YES;
}
@end
