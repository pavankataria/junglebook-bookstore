//
//  BookEntry.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BookEntry : NSManagedObject

@property (nonatomic, retain) NSString * bookTitle;
@property (nonatomic, retain) NSString * bookAuthor;
@property (nonatomic, retain) NSString * bookPrice;
@property (nonatomic, retain) NSString * bookRating;
@property (nonatomic, retain) NSString * bookCoverThumbnailImageURL;
@property (nonatomic, retain) NSString * bookCoverOriginalImageURL;
@property (nonatomic, retain) NSData * bookCoverThumbnailImage;
@property (nonatomic, retain) NSData * bookCoverOriginalImage;

@end
