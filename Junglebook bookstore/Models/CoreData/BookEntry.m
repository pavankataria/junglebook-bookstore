//
//  BookEntry.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "BookEntry.h"


@implementation BookEntry

@dynamic bookTitle;
@dynamic bookAuthor;
@dynamic bookPrice;
@dynamic bookRating;
@dynamic bookCoverThumbnailImageURL;
@dynamic bookCoverOriginalImageURL;
@dynamic bookCoverThumbnailImage;
@dynamic bookCoverOriginalImage;

@end
