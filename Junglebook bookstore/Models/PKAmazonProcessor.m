//
//  PKAmazonProcessor.m
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import "PKAmazonProcessor.h"

@implementation PKAmazonProcessor



+(NSString*)getBookTitleWithArray:(NSArray*)array{
    return [[array[0] objectForKey:kAmazonAHREFKey] objectForKey:kAmazonUnderscoreTextKey];
}

+(NSString*)getBookAuthorWithArray:(NSArray*)array{
    id bookAuthor = [[array[1] objectForKey:kAmazonAHREFKey] objectForKey:kAmazonUnderscoreTextKey];
    
    if(!bookAuthor){
        bookAuthor = [array[1] objectForKey:kAmazonUnderscoreTextKey];
    }
    
    if([bookAuthor isKindOfClass:[NSArray class]]){
        bookAuthor = [bookAuthor componentsJoinedByString:@" "];
    }
    
    return bookAuthor;
}
+(NSString*)getPriceFromDictionary:(NSDictionary*)dictionary{
    return [NSString stringWithUTF8String:[[[[dictionary objectForKey:@"font"] lastObject] objectForKey:@"b"] cStringUsingEncoding:NSUTF8StringEncoding]];
}


+(NSString*)getRatingWithCurrentRatingDictionary:(NSDictionary*)ratingDictionary{
    NSString * stars;
    if([ratingDictionary objectForKey:@"_src"]){
        NSString * possibleStarsURL = [ratingDictionary objectForKey:@"_src"];
        if([possibleStarsURL rangeOfString:@"stars-" options:NSCaseInsensitiveSearch].location != NSNotFound){
            stars = [[[[[possibleStarsURL componentsSeparatedByString:@"stars-"] lastObject] componentsSeparatedByString:@"."] firstObject] stringByReplacingOccurrencesOfString:@"-" withString:@"."];
        }
    }
    
    
    return stars;
    
}
+(NSString*)getRatingFromDictionary:(NSDictionary*)dictionary{
    id currentDictionary = [dictionary objectForKey:@"img"];
    NSString *rating;
    
    if([currentDictionary isKindOfClass:[NSArray class]]){
        for(int i = 0; i < [currentDictionary count]; i++){
            NSDictionary *currentRatingDictionary = [currentDictionary objectAtIndex:i];
            
            if((rating = [self getRatingWithCurrentRatingDictionary:currentRatingDictionary])){
                break;
            }
        }
    }
    
    else if([currentDictionary isKindOfClass:[NSDictionary class]]){
        rating = [self getRatingWithCurrentRatingDictionary:currentDictionary];
    }
    
    if(!rating) rating = @"Rating is not currently available";
    return rating;
}


+(NSArray*)processAmazonResponseWithXMLData:(NSData*)responseObject{
    NSMutableArray *bookEntries = [[NSMutableArray alloc] init];
    
    NSDictionary * itemDictionary = [[NSDictionary dictionaryWithXMLData:responseObject] objectForKey:kAmazonRootNode];
    for(int i = 0; i < [[itemDictionary objectForKey:kAmazonFeedItemKey] count]; i++){
        RSSBookEntryModel *cBEO = [[RSSBookEntryModel alloc] init];

        NSDictionary *currentItem = [[itemDictionary objectForKey:kAmazonFeedItemKey] objectAtIndex:i];
        NSString *finalXMLString = [NSString stringWithFormat:@"%@%@%@", kAmazonStartTag, [currentItem objectForKey:kAmazonDescriptionKey], kAmazonEndTag];
        NSDictionary *cData = [NSDictionary dictionaryWithXMLString:finalXMLString];
        
        NSArray *bookDetailsDictionary = [cData objectForKey:kAmazonSpanKey];
        
        NSString *bIOURL = [[[[cData objectForKey:@"div"] objectForKey:kAmazonAHREFKey] objectForKey:@"img"] objectForKey:@"_src"];
        NSString *bookImageCoverID = [[[[bIOURL componentsSeparatedByString:kAmazonBookCoverBaseURL] lastObject] componentsSeparatedByString:@"."] firstObject];

        
        
        
        cBEO.bookTitle = [self getBookTitleWithArray:bookDetailsDictionary];
        cBEO.bookAuthor = [self getBookAuthorWithArray:bookDetailsDictionary];
        cBEO.bookCoverImageThumbnailURL = [NSString stringWithFormat:@"%@%@%@%@", kAmazonBookCoverBaseURL, bookImageCoverID, kAmazonBookCoverThumbnailSize, kAmazonBookCoverFileExtention];
        cBEO.bookCoverImageOriginalURL = [NSString stringWithFormat:@"%@%@%@%@", kAmazonBookCoverBaseURL, bookImageCoverID, kAmazonBookCoverMaxSize, kAmazonBookCoverFileExtention];
        cBEO.bookPrice = [self getPriceFromDictionary:cData];
        cBEO.bookRating = [self getRatingFromDictionary:cData];

        [bookEntries addObject:cBEO];
        
        
    }
    return bookEntries;
}
@end
