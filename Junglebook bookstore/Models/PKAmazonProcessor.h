//
//  PKAmazonProcessor.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSBookEntryModel.h"

@interface PKAmazonProcessor : NSObject
+(NSArray*)processAmazonResponseWithXMLData:(NSData*)responseObject;
@end
