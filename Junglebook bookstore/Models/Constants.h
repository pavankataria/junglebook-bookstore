//
//  Constants.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//
#import <Foundation/Foundation.h>

#ifndef Junglebook_bookstore_Constants_h
#define Junglebook_bookstore_Constants_h
#endif



#ifdef DEBUG
#define Key(class, key)              ([(class *)nil key] ? @#key : @#key)
#define ProtocolKey(protocol, key)   ([(id <protocol>)nil key] ? @#key : @#key)
#else
#define Key(class, key)              @#key
#define ProtocolKey(class, key)      @#key
#endif

//Amazon constants
extern NSString * const kAmazonDefaultFeedURLString;
extern NSString * const kAmazonRootNode;
extern NSString * const kAmazonStartTag;
extern NSString * const kAmazonEndTag;

extern NSString * const kAmazonFeedItemKey;
extern NSString *const kAmazonSpanKey;
extern NSString * const kAmazonDescriptionKey;
extern NSString *const kAmazonUnderscoreTextKey;
extern NSString *const kAmazonAHREFKey;
extern NSString *const kAmazonBookCoverBaseURL;

extern NSString *const kAmazonBookCoverThumbnailSize;
extern NSString *const kAmazonBookCoverMaxSize;
extern NSString *const kAmazonBookCoverFileExtention;


//Jungle Book constants
extern NSString * const kJungleBookIdentifier;





