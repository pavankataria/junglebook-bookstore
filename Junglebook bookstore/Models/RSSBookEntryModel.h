//
//  RSSBookEntryModel.h
//  Junglebook bookstore
//
//  Created by Pavan Kataria on 10/11/2014.
//  Copyright (c) 2014 Pavan Kataria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RSSBookEntryModel : NSObject<NSXMLParserDelegate>

@property (strong, nonatomic) NSString *bookTitle;
@property (strong, nonatomic) NSString *bookAuthor;
@property (strong, nonatomic) NSString *bookCoverImageThumbnailURL;
@property (strong, nonatomic) NSString *bookCoverImageOriginalURL;
@property (strong, nonatomic) NSData *bookCoverThumbnailImage;
@property (strong, nonatomic) NSData *bookCoverOriginalImage;

@property (strong, nonatomic) NSString *bookPrice;
@property (strong, nonatomic) NSString *bookRating;


-(NSString*)description;

@end
